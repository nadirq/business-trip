'use strict';
// Ссылка на серверную часть приложения
var serviceBase = 'http://localhost:8080';

// Основной модуль приложения и его компоненты
var businessTrip = angular.module('businessTrip', [
    'ngRoute',
    'businessTrip.department'
]);

// рабочий модуль
var businessTrip_department = angular.module('businessTrip.department', ['ngRoute']);

businessTrip.config(['$routeProvider', function($routeProvider) {
    $routeProvider.otherwise({redirectTo: '/departments'});
}]);