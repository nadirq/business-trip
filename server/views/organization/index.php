<?php

use yii\grid\GridView;
use yii\helpers\Html;

/**
 * @var $this         yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$this->title                   = 'Organizations list';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organization-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Html::a('Create Organization', ['create'], ['class' => 'btn btn-success']) ?></p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            'name',
            'city_id',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
