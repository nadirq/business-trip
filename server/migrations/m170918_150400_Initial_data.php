<?php

use yii\db\Migration;

/**
 * Initial data
 * ATTENTION: it's a bad idea to fill the database with migration
 *
 * Class m170918_150400_Initial_data
 */
class m170918_150400_Initial_data extends Migration
{
    public function safeUp()
    {
        $this->insert('department', [
            'name' => 'Administrative',
        ]);

        $this->insert('worker', [
            'name'          => 'Alexander Azimov',
            'position'      => 'CEO',
            'role'          => 1,
            'login'         => 'alex',
            'password'      => sha1('12345'),
            'department_id' => 1,
        ]);
    }

    public function safeDown()
    {
    }
}
