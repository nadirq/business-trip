<?php

use yii\db\Migration;

/**
 * Initial migration
 *
 * Class m170918_134005_Initial
 */
class m170918_134005_Initial extends Migration
{
    public function safeUp()
    {
        $this->createTable('department', [
            'id'   => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
        ]);

        $this->createTable('worker', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(255)->notNull(),
            'position'      => $this->string(),
            'role'          => $this->string(50)->notNull(),
            'login'         => $this->string(50)->notNull(),
            'password'      => $this->string(255)->notNull(),
            'department_id' => $this->integer()->notNull(),
        ]);

        $this->createTable('city', [
            'id'   => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
        ]);

        $this->createTable('organization', [
            'id'      => $this->primaryKey(),
            'name'    => $this->string(255)->notNull(),
            'city_id' => $this->integer()->notNull(),
        ]);

        $this->createTable('business_trip', [
            'id'              => $this->primaryKey(),
            'description'     => $this->text(),
            'start_time'      => $this->timestamp(),
            'status_active'   => $this->boolean()->defaultValue(true),
            'worker_id'       => $this->integer()->notNull(),
            'organization_id' => $this->integer()->notNull(),
        ]);

        $this->createTable('expense', [
            'id'               => $this->primaryKey(),
            'description'      => $this->text(),
            'sum'              => $this->float()->notNull(),
            'business_trip_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-worker-department_id',
            'worker',
            'department_id'
        );

        $this->addForeignKey(
            'fk-worker-department_id',
            'worker',
            'department_id',
            'department',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        $this->createIndex(
            'idx-organization-city_id',
            'organization',
            'city_id'
        );

        $this->addForeignKey(
            'fk-organization-city_id',
            'organization',
            'city_id',
            'city',
            'id',
            'RESTRICT',
            'CASCADE'
        );


        $this->createIndex(
            'idx-business_trip-worker_id',
            'business_trip',
            'worker_id'
        );

        $this->addForeignKey(
            'fk-business_trip-worker_id',
            'business_trip',
            'worker_id',
            'worker',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
            'idx-business_trip-organization_id',
            'business_trip',
            'organization_id'
        );

        $this->addForeignKey(
            'fk-business_trip-organization_id',
            'business_trip',
            'organization_id',
            'organization',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        $this->createIndex(
            'idx-expense-business_trip_id',
            'expense',
            'business_trip_id'
        );

        $this->addForeignKey(
            'fk-expense-business_trip_id',
            'expense',
            'business_trip_id',
            'business_trip',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-worker-department_id', 'worker');
        $this->dropIndex('idx-worker-department_id', 'worker');
        $this->dropForeignKey('fk-organization-city_id', 'organization');
        $this->dropIndex('idx-organization-city_id', 'organization');
        $this->dropForeignKey('fk-business_trip-worker_id', 'business_trip');
        $this->dropIndex('idx-business_trip-worker_id', 'business_trip');
        $this->dropForeignKey('fk-business_trip-organization_id', 'business_trip');
        $this->dropIndex('idx-business_trip-organization_id', 'business_trip');
        $this->dropForeignKey('fk-expense-business_trip_id', 'expense');
        $this->dropIndex('idx-expense-business_trip_id', 'expense');

        $this->dropTable('city');
        $this->dropTable('department');
        $this->dropTable('expense');
        $this->dropTable('business_trip');
        $this->dropTable('organization');
        $this->dropTable('worker');
    }
}
