<?php

namespace app\controllers;

use yii\filters\Cors;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;

/**
 * DepartmentController implements the CRUD actions for Department model.
 */
abstract class ApiController extends ActiveController
{
    /**
     * You need to redeclare this property
     *
     * @var string
     */
    public $modelClass;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return
            ArrayHelper::merge(parent::behaviors(), [
                'corsFilter' => [
                    'class' => Cors::className(),
                ],
                'verbs'      => [
                    'class'   => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]);
    }
}