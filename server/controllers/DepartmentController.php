<?php

namespace app\controllers;

/**
 * DepartmentController implements the CRUD actions for Department model.
 */
class DepartmentController extends ApiController
{
    public $modelClass = "app\models\Department";
}
