<?php

namespace app\models;

/**
 * This is the model class for table "business_trip".
 *
 * @property integer      $id
 * @property string       $description
 * @property string       $start_time
 * @property integer      $status_active
 * @property integer      $worker_id
 * @property integer      $organization_id
 *
 * @property Organization $organization
 * @property Worker       $worker
 * @property Expense[]    $expenses
 */
class BusinessTrip extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'business_trip';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['start_time'], 'safe'],
            [['status_active', 'worker_id', 'organization_id'], 'integer'],
            [['worker_id', 'organization_id'], 'required'],
            [['organization_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organization::className(), 'targetAttribute' => ['organization_id' => 'id']],
            [['worker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Worker::className(), 'targetAttribute' => ['worker_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'description'     => 'Description',
            'start_time'      => 'Start Time',
            'status_active'   => 'Status Active',
            'worker_id'       => 'Worker ID',
            'organization_id' => 'Organization ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganization()
    {
        return $this->hasOne(Organization::className(), ['id' => 'organization_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenses()
    {
        return $this->hasMany(Expense::className(), ['business_trip_id' => 'id']);
    }
}
