<?php

namespace app\models;

/**
 * This is the model class for table "expense".
 *
 * @property integer      $id
 * @property string       $description
 * @property double       $sum
 * @property integer      $business_trip_id
 *
 * @property BusinessTrip $businessTrip
 */
class Expense extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expense';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['sum', 'business_trip_id'], 'required'],
            [['sum'], 'number'],
            [['business_trip_id'], 'integer'],
            [['business_trip_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessTrip::className(), 'targetAttribute' => ['business_trip_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => 'ID',
            'description'      => 'Description',
            'sum'              => 'Sum',
            'business_trip_id' => 'Business Trip ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusinessTrip()
    {
        return $this->hasOne(BusinessTrip::className(), ['id' => 'business_trip_id']);
    }
}