<?php

namespace app\models;

/**
 * This is the model class for table "worker".
 *
 * @property integer        $id
 * @property string         $name
 * @property string         $position
 * @property string         $role
 * @property string         $login
 * @property string         $password
 * @property integer        $department_id
 *
 * @property BusinessTrip[] $businessTrips
 * @property Department     $department
 */
class Worker extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'worker';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'role', 'login', 'password', 'department_id'], 'required'],
            [['department_id'], 'integer'],
            [['name', 'position', 'password'], 'string', 'max' => 255],
            [['role', 'login'], 'string', 'max' => 50],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['department_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'name'          => 'Name',
            'position'      => 'Position',
            'role'          => 'Role',
            'login'         => 'Login',
            'password'      => 'Password',
            'department_id' => 'Department ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusinessTrips()
    {
        return $this->hasMany(BusinessTrip::className(), ['worker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    public function beforeSave($insert)
    {
        $this->password = sha1($this->password);

        return parent::beforeSave($insert);
    }
}